#!/usr/bin/env python3
''' Programm um die bagit Struktur zu beseitigen '''
import argparse, logging, os, re, shutil


parser= argparse.ArgumentParser(description='entfernt die bagit Struktur')
parser.add_argument('-v', '--verbose', action='count', default=0, help='warning, info, debug')
parser.add_argument('-I', action='store_true', help='intermediat Ordner entfernen')
parser.add_argument('Verzeichnis',  help='Verzeichnis enthält bagit Struktur')

args= parser.parse_args()


levels = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
level = levels[min(len(levels)-1,args.verbose)]
logging.basicConfig(level=level, format="%(asctime)s %(levelname)s %(filename)s [%(lineno)d] %(message)s")
logger = logging.getLogger(__name__)

logger.debug("args {}".format(vars(args)))

verzeichnis= vars(args)['Verzeichnis']
if os.path.isdir(verzeichnis):
    if vars(args)['I']:
        verzeichnisAlt= verzeichnis+'_alt_'
        logger.debug("mv {} {}".format(verzeichnis,verzeichnisAlt))
        os.rename(verzeichnis,verzeichnisAlt)
        dataVerzeichnis= os.path.join(verzeichnisAlt,'data')
        for verzeichnisNeu in os.listdir(dataVerzeichnis):
            if os.path.isdir(os.path.join(dataVerzeichnis,verzeichnisNeu)):
                logger.debug("mv {} {}".format(os.path.join(dataVerzeichnis,verzeichnisNeu),verzeichnisNeu))
                os.rename(os.path.join(dataVerzeichnis,verzeichnisNeu),verzeichnisNeu)
        logger.debug("rmdir -r {}".format(verzeichnisAlt))
        # gefaerlich sicherstellen das kein langer Pfad
        if verzeichnisAlt == os.path.split(verzeichnisAlt)[1]:
            shutil.rmtree(os.path.split(verzeichnisAlt)[1])
        else:
            print("{} muss mit Hand entfernt werden".format(verzeichnisAlt))
    else:
        for bagTxtDatei in [f for f in os.listdir(verzeichnis) if re.match(r"^(bag-?i|(tag)?manifest-).*\.txt$", f)]:
            if os.path.isfile(os.path.join(verzeichnis,bagTxtDatei)) :
                logger.debug("rm {}".format(os.path.join(verzeichnis,bagTxtDatei)))
                os.remove(os.path.join(verzeichnis,bagTxtDatei))
        dataVerzeichnis= os.path.join(verzeichnis,'data')
        if os.path.isdir(dataVerzeichnis):
            for datei in os.listdir(dataVerzeichnis):
                logger.debug("mv {} {}".format(os.path.join(dataVerzeichnis,datei), os.path.join(verzeichnis,os.path.split(datei)[1])))
                os.rename(os.path.join(dataVerzeichnis,datei), os.path.join(verzeichnis,os.path.split(datei)[1]))
            logger.debug("rmdir {}".format(str(dataVerzeichnis)))
            os.rmdir(dataVerzeichnis)
